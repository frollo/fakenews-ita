from pymongo import MongoClient

def uncurse(f):
    def wrapper(el):
        cursor = f(el)
        return [v for v in cursor]
    return wrapper

class Analist(object):
    def __init__(self):
        super(Analist, self).__init__()
        client = MongoClient("localhost", 27017)
        self.db = client.fakenewsita

    @uncurse
    def countByCat(self):
        return self.db.articles.aggregate([
            {"$group" : {"_id" : "$cat", "count" : {"$sum" : 1}}}
        ])

    @uncurse
    def sharesByCat(self):
        return self.db.articles.aggregate([
                {"$unwind" : {"path" : "$shares"}},
                {"$group" : {"_id" : "$cat", "shares" : { "$push" : "$shares" }}},
                {"$project" : {
                    "Facebook" : {
                        "$filter" : {
                            "input" : "$shares",
                            "as" : "shares",
                            "cond" : {"$eq" : ["$$shares.social", "Facebook"]}
                        }
                    },
                    "LinkedIn" : {
                        "$filter": {
                            "input" : "$shares",
                            "as" : "shares",
                            "cond" : {"$eq" : ["$$shares.social", "LinkedIn"]}
                        }
                    }
                }},
                {"$project" : {
                    "Facebook" : {
                        "$reduce" : {
                            "input" : "$Facebook",
                            "initialValue" : 0,
                            "in" : {"$add" : ["$$value", "$$this.count"]}
                        }
                    },
                    "LinkedIn" : {
                        "$reduce" : {
                            "input" : "$LinkedIn",
                            "initialValue" : 0,
                            "in" : {"$add" : ["$$value", "$$this.count"]}
                        }
                    }
                }}
        ])

    def groupByDay(self):
        dayByDay = self.db.articles.aggregate([
            {"$unwind" : {"path" : "$shares"}},
            {"$project" : {
                "date" : 1,
                "cat" : 1,
                "type" : "$shares.social",
                "count" : "$shares.count"
            }},
            {"$group" : {
                "_id" : { "month": { "$month": "$date" }, "day": { "$dayOfMonth": "$date" }, "year": { "$year": "$date" } },
                "data" : {"$push" : {"cat" : "$cat", "type" : "$type", "count" : "$count"}}
            }},
            {"$project" : {
                "fake" : {
                    "$filter" : {
                        "input" : "$data",
                        "as" : "data",
                        "cond" : {"$eq" : ["$$data.cat", "fake"]}
                    }
                },
                "legitimate" : {
                    "$filter" : {
                        "input" : "$data",
                        "as" : "data",
                        "cond" : {"$eq" : ["$$data.cat", "legitimate"]}
                    }
                }
            }}
        ])
        processedDays = []
        for day in dayByDay:
            flinkedin = 0
            ffacebook = 0
            fakes = day["fake"]
            farticles = len(fakes) / 2
            for f in fakes:
                if f["type"] == "LinkedIn":
                    flinkedin += f["count"]
                elif f["type"] == "Facebook":
                    ffacebook += f["count"]
            llinkedin = 0
            lfacebook = 0
            legits = day["legitimate"]
            larticles = len(legits) /2
            for l in legits:
                if l["type"] == "LinkedIn":
                    llinkedin += l["count"]
                elif l["type"] == "Facebook":
                    lfacebook += l["count"]
            processedDays.append({
                "_id" : "{0}/{1}/{2}".format(day["_id"]["day"], day["_id"]["month"], day["_id"]["year"]),
                "fakes" : {
                    "articles" : farticles,
                    "Facebook" : ffacebook,
                    "LinkedIn" : flinkedin
                },
                "legitimates" : {
                    "articles" : larticles,
                    "Facebook" : lfacebook,
                    "LinkedIn" : llinkedin
                }
            })

        return processedDays
