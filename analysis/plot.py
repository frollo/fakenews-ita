from matplotlib import pyplot as plt
import numpy as np
from matplotlib.ticker import MaxNLocator
from collections import namedtuple
from datetime import datetime

class Plotter(object):
    def __init__(self, analist):
        self.__analist__ = analist

    def plotCats(self,figname):
        data = self.__analist__.countByCat()
        tot = sum([x["count"] for x in data])
        labels = []
        percs = []
        for x in data:
            labels.append(x["_id"])
            percs.append(((x["count"] * 1.0) / tot) * 100.0)
        plt.pie(percs, labels=labels,shadow=True)
        plt.savefig(figname)

    def plotShares(self, fignameFB, fignameLK, fignameLegit, fignameFake):
        data = self.__analist__.sharesByCat()
        fb = [{"cat" : c["_id"], "value" : c["Facebook"]} for c in data]
        lk = [{"cat" : c["_id"], "value" : c["LinkedIn"]} for c in data]
        totFB = sum([x["value"] for x in fb])
        totLK = sum([x["value"] for x in lk])

        #Facebook (fake vs legitimate)
        FBLabels = []
        FBPercs = []
        for f in fb:
            FBLabels.append(f["cat"])
            FBPercs.append(((f["value"] * 1.0)/totFB) * 100.0)
        plt.pie(FBPercs, labels=FBLabels, shadow=True)
        plt.savefig(fignameFB)
        plt.close()

        #LinkedIn (fake vs legitimate)
        LKLabels = []
        LKPercs = []
        for l in lk:
            LKLabels.append(l["cat"])
            LKPercs.append((l["value"] * 1.0)/totLK * 100.0)
        plt.pie(LKPercs, labels=LKLabels, shadow=True)
        plt.savefig(fignameLK)
        plt.close()

        #Legit (facebook vs linkedIn)
        legit = [x for x in data if x["_id"] == "legitimate"][0]
        totLegit = legit["Facebook"] + legit["LinkedIn"]
        lLabels = ["Facebook", "LinkedIn"]
        lData = [(((legit["Facebook"] * 1.0)/totLegit) * 100.0), (((legit["LinkedIn"] * 1.0)/totLegit) * 100.0)]
        plt.pie(lData, labels=lLabels, shadow=True)
        plt.savefig(fignameLegit)
        plt.close()

        #Fake (facebook vs linkedin)
        fake = [x for x in data if x["_id"] == "fake"][0]
        totFake = fake["Facebook"] + fake["LinkedIn"]
        fLabels = ["Facebook", "LinkedIn"]
        fData = [(((fake["Facebook"] * 1.0)/totFake) * 100.0), (((fake["LinkedIn"] * 1.0)/totFake) * 100.0)]
        plt.pie(fData, labels=fLabels, shadow=True)
        plt.savefig(fignameFake)
        plt.close()

    def plotDays(self, figname):
        data = self.__analist__.groupByDay()
        data.sort(key=lambda x : datetime.strptime(x["_id"], "%d/%m/%Y"))
        legitArticles = tuple([x["legitimates"]["articles"] for x in data])
        fakeArticles = tuple([x["fakes"]["articles"] for x in data])
        tickLabels = tuple([x["_id"] for x in data])
        n_groups = len(data)

        fig, ax = plt.subplots()
        index = np.arange(n_groups)
        bar_width = 0.25
        opacity = 0.8
        legitRects = ax.bar(index, legitArticles,bar_width,alpha=opacity,color="b", label="Legit")
        fakeRects = ax.bar(index + bar_width, fakeArticles,bar_width, alpha=opacity, color="r",label="Fake")
        ax.set_xlabel("Giorni")
        ax.set_ylabel("Articoli")
        ax.set_xticks(index + bar_width / 2)
        ax.set_xticklabels(tickLabels)
        ax.legend()
        fig.set_size_inches(13,6)
        fig.tight_layout()
        plt.savefig(figname)
        plt.close()
