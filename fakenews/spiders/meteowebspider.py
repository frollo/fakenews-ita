import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}

class MeteoWebSpider(scrapy.Spider):
    name = 'meteowebspider'
    start_urls = ['http://www.meteoweb.eu/category/altre-scienze/medicina-salute/',
        'http://www.meteoweb.eu/category/geofisica-vulcanologia/',
        'http://www.meteoweb.eu/category/astronomia/',
        'http://www.meteoweb.eu/category/oltre-la-scienza/'
    ]

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[1]]
        d = int(splitString[0])
        y = int(splitString[2])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('div.td_module_5.td_module_wrap.td-animation-stack'):
            title = article.css('h3.entry-title.td-module-title')
            datestring = article.css('div.meta-info').css("time ::text").extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = self.__getDate__(datestring)
            item['cat'] = 'fake'
            yield item
