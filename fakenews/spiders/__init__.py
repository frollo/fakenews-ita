# This package will contain the spiders of your Scrapy project
#
# Please refer to the documentation for information on how to create and manage
# your spiders.

from aprilamentespider import AprilaMenteSpider
from dionidreamspider import DionidreamSpider
from iuxsspider import IUxSSpider
from lescienzespider import LeScienzeSpider
from medbunkerspider import MedbunkerSpider
from meteowebspider import MeteoWebSpider
from nexusedizionispider import NexusEdizioniSpider
from pianetabluspider import PianetaBluSpider
from queryspider import QuerySpider
from segnidalcielospider import SegniDalCieloSpider
from zeusspider import ZeusSpider
