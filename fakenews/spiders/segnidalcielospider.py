import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

class SegniDalCieloSpider(scrapy.Spider):
    name = 'segnidalcielospider'
    start_urls = ['http://www.segnidalcielo.it/category/avvistamenti-ufo-misteri-fatti-inspiegabili/',
        'http://www.segnidalcielo.it/category/nuovo-ordine-mondiale/geoingegneria/',
        'http://www.segnidalcielo.it/category/medicina/']

    def parse(self, response):
        for article in response.css('header.entry-header'):
            title = article.css('h2.entry-title')
            datestring = article.css('div.entry-byline').css('time ::text').extract_first()
            date = datetime.strptime(datestring, "%d/%m/%Y")
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = date
            item['cat'] = 'fake'
            yield item
