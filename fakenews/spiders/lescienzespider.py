import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}

class LeScienzeSpider(scrapy.Spider):
    name = 'lescienzespider'
    start_urls = ['http://pasini-lescienze.blogautore.espresso.repubblica.it/',
        'http://bressanini-lescienze.blogautore.espresso.repubblica.it/',
        'http://rudimatematici-lescienze.blogautore.espresso.repubblica.it/',
        'http://oldani-lescienze.blogautore.espresso.repubblica.it/',
        'http://digrazia-lescienze.blogautore.espresso.repubblica.it/',
        'http://ovadia-lescienze.blogautore.espresso.repubblica.it',
        'http://mautino-lescienze.blogautore.espresso.repubblica.it/'
    ]

    def parse(self, response):
        for article in response.css('div.post'):
            title = article.css('header').css('h1')
            dateBase = article.css('aside.colblog').css('div.datapubb-blog')
            day = int(dateBase.css('strong ::text').extract_first())
            month = months[dateBase.css('span:not([class="colblog-year"]) ::text').extract_first()]
            year = int(dateBase.css('span.colblog-year ::text').extract_first())
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = datetime(year, month, day)
            item['cat'] = 'legitimate'
            yield item
