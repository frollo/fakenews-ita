import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}

class DionidreamSpider(scrapy.Spider):
    name = 'dionidreamspider'
    start_urls = ['https://www.dionidream.com/']

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[0]]
        d = int(splitString[1][0:-1])
        y = int(splitString[2])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('div.jeg_postblock_content'):
            title = article.css('h3.jeg_post_title')
            datestring = article.css('div.jeg_post_meta').css("div.jeg_post_meta").css('a ::text').extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = self.__getDate__(datestring)
            item['cat'] = 'fake'
            yield item
