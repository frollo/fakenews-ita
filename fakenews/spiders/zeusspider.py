import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}

class ZeusSpider(scrapy.Spider):
    name = 'zeusspider'
    start_urls = ['https://www.zeusgenova.it/']

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[1]]
        d = int(splitString[0])
        y = int(splitString[2])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('article.post.status-publish'):
            title = article.css('header.entry-header').css('h2.entry-title')
            datestring = article.css('div.entry-body').css('div.entry-meta').css('span.posted-on').css('a').css('time.entry-date.published ::text').extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = self.__getDate__(datestring)
            item['cat'] = 'legitimate'
            yield item
