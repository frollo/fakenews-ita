import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

class AprilaMenteSpider(scrapy.Spider):
    name = 'aprilamentespider'
    start_urls = ['http://aprilamente.info/category/scienza/',
        'http://aprilamente.info/category/medicina/',
        'http://aprilamente.info/category/fisica-quantistica/']

    def parse(self, response):
        for article in response.css('div.td_module_1.td_module_wrap.td-animation-stack'):
            title = article.css('h3.entry-title.td-module-title')
            datestring = article.css('div.td-module-meta-info').css("span.td-post-date").css('time ::text').extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = datetime.strptime(datestring, "%d/%m/%Y")
            item['cat'] = 'fake'
            yield item
