import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}


class PianetaBluSpider(scrapy.Spider):
    name = 'pianetabluspider'
    start_urls = ['http://www.pianetablunews.it/category/senza-categoria/']

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[1]]
        d = int(splitString[0])
        y = int(splitString[2])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('article'):
            title = article.css('h2.entry-title.post-title')
            datestring = article.css('div.entry-meta.postmeta').css("span.meta-date").css('a').css("time ::text").extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = self.__getDate__(datestring)
            item['cat'] = 'fake'
            yield item
