import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "Jan" : 1,
    "Feb" : 2,
    "Mar" : 3,
    "Apr" : 4,
    "May" : 5,
    "Jun" : 6,
    "Jul" : 7,
    "Aug" : 8,
    "Sep" : 9,
    "Oct" : 10,
    "Nov" : 11,
    "Dec" : 12
}

class NexusEdizioniSpider(scrapy.Spider):
    name = 'nexusedizionispider'
    start_urls = ['http://www.nexusedizioni.it/it/']

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[1]]
        d = int(splitString[0])
        y = int(splitString[2])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('div.blog-desc'):
            title = article.css('a')
            linkArticle = article.css('a ::attr(href)').extract_first
            datestring = article.re(r'\d\d [A-Z][a-z]{2} \d{4}')
            if len(datestring) == 0:
                continue
            item = FakenewsItem()
            item['title'] = title.css('h4.mt-10 ::text').extract_first()
            item['link'] = "http://www.nexusedizioni.it{0}".format(linkArticle)
            item['date'] = self.__getDate__(datestring[0])
            item['cat'] = 'fake'
            yield item
