import scrapy
from datetime import datetime
from fakenews.items import FakenewsItem

months = {
    "gennaio" : 1,
    "febbraio" : 2,
    "marzo" : 3,
    "aprile" : 4,
    "maggio" : 5,
    "giugno" : 6,
    "luglio" : 7,
    "agosto" : 8,
    "settembre" : 9,
    "ottobre" : 10,
    "novembre" : 11,
    "dicembre" : 12
}

class MedbunkerSpider(scrapy.Spider):
    name = 'medbunkerspider'
    start_urls = ['http://medbunker.blogspot.it/']

    def __getDate__(self, string):
        splitString = string.split()
        m = months[splitString[2]]
        d = int(splitString[1])
        y = int(splitString[3])
        return datetime(y,m,d)

    def parse(self, response):
        for article in response.css('div.date-outer'):
            title = article.css('div.date-posts > div.post-outer > div.post.hentry > h3.post-title.entry-title')
            datestring = article.css('h2.date-header > span ::text').extract_first()
            item = FakenewsItem()
            item['title'] = title.css('a ::text').extract_first()
            item['link'] = title.css('a ::attr(href)').extract_first()
            item['date'] = self.__getDate__(datestring)
            item['cat'] = 'legitimate'
            yield item
