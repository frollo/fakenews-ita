# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from pymongo import MongoClient
from scrapy.exceptions import DropItem
import requests
from datetime import datetime
from items import FakenewsItem
from time import sleep

class Shares(object):
    targets = [
        {
            "name" : "LinkedIn",
            "address" : "http://www.linkedin.com/countserv/count/share?url={0}&format=json",
            "extract" : lambda x : x["count"]
        },
        {
            "name" : "Facebook",
            "address" : "https://graph.facebook.com/?id={0}",
            "extract" : lambda x : x["share"]["share_count"]
        }
    ]

    @classmethod
    def from_crawler(cls, crawler):
        return cls()

    def open_spider(self, spider):
        pass

    def close_spider(self, spider):
        pass

    def process_item(self, item, spider):
        sleep(5)
        item["shares"] = []
        for target in self.targets:
            tData = requests.get(target["address"].format(item["link"])).json()
            try:
                tShares = target["extract"](tData)
            except Exception as e:
                print "Could not find data for {0} on {1}.Got\n{2}\n====\n".format(item["title"], target["name"], tData)
                tShares = 0
            item["shares"].append({"social" : target["name"], "count" : tShares})
        return item


class MongoFilter(object):
    collection_name = 'articles'
    def __init__(self):
        self.mongo_uri = "localhost"
        self.mongo_db = "fakenewsita"

    @classmethod
    def from_crawler(cls, crawler):
        return cls()

    def open_spider(self, spider):
        self.client = MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        if self.db[self.collection_name].find({"link" : item["link"]}).count() > 0:
            raise DropItem("Duplicate item found: %s" % item)
        elif item["date"] < datetime(2018,1,1):
            raise DropItem("Item too old %s" % item["date"])
        else:
            return item

class MongoPipeline(object):
    collection_name = 'articles'

    def __init__(self):
        self.mongo_uri = "localhost"
        self.mongo_db = "fakenewsita"

    @classmethod
    def from_crawler(cls, crawler):
        return cls()

    def open_spider(self, spider):
        self.client = MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()

    def process_item(self, item, spider):
        self.db[self.collection_name].insert_one(dict(item))
        return item
