from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
import logging
import sys
import inspect
from fakenews import spiders
from analysis import Plotter, Analist
# Crawling

def crawlAll():
    spiders = [x[1].name for x in inspect.getmembers(sys.modules['fakenews.spiders'], inspect.isclass)]
    process = CrawlerProcess(get_project_settings())
    for spider in spiders:
        process.crawl(spider)
    process.start() # the script will block here until the crawling is finished


if __name__ == '__main__':
    #set up scrapy settings
    logging.getLogger('scrapy').setLevel(logging.WARNING)
    logging.getLogger('scrapy').propagate = False
    #crawl
    crawlAll()

    #analysis and plotting
    analist = Analist()
    plotter = Plotter(analist)
    plotter.plotCats("../imgs/cats.png")
    plotter.plotShares("../imgs/sharesFB.png", "../imgs/sharesLK.png", "../imgs/sharesLegit.png", "../imgs/sharesFake.png")
    plotter.plotDays("../imgs/days.png")
